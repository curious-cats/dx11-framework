#ifndef _FRAMEWORK_H
#define _FRAMEWORK_H

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>

class Framework {

public:
	Framework(void);
	~Framework(void);

	bool Initalize(void);
	void Run(void);

private:
	char* m_applicationName;
	HINSTANCE m_hInstance;

	bool CreateDXWindow(char* windowTitle, int x, int y, int width, int height);

};

#endif