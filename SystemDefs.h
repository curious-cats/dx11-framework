#ifndef _SYSTEM_DEFS_H
#define _SYSTEM_DEFS_H

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define WINDOW_POSX 0
#define WINDOW_POSY 0

const float SCREEN_DEPTH = 1000.0f;
const float SCREEN_NEAR = 0.1f;

#endif