#include "Framework.h"

void main() {

	Framework* framework = new Framework();

	bool init = framework->Initialize();

	if (init) framework->Run();

	return;

}